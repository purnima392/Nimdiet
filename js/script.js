(function ($) {
	$(document).ready(function () {
		$(window).on('scroll', function () {
			var scroll = $(window).scrollTop();

			if (scroll >= 10) {
				$('.navbar-default').addClass('affix');
			} else {
				$('.navbar-default').removeClass('affix');
			}
		});
		
		//Owl carousel
		//-----------------------------------------------

		$('.owl-carousel').owlCarousel({
			loop: true,
			autoplay: true,
			margin: 10,
			responsiveClass: true,
			responsive: {
				0: {
					items: 1,
					nav: true
				},
				600: {
					items: 3,
					nav: false
				},
				1000: {
					items: 4,
					nav: true,
					navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
					loop: false,
					margin: 20
				}
			}
		})

		//Scroll totop
		//-----------------------------------------------
		$(window).scroll(function () {
			if ($(this).scrollTop() != 0) {
				$(".scrollToTop").fadeIn();
			} else {
				$(".scrollToTop").fadeOut();
			}
		});
		$(".scrollToTop").click(function () {
			$("body,html").animate({
				scrollTop: 0
			}, 800);
		});
		$(function () { var b = "fadeInRight"; var c; var a; d($("#myTab a"), $(".tab-content")); function d(e, f, g) { e.click(function (i) { i.preventDefault(); $(this).tab("show"); var h = $(this).data("easein"); if (c) { c.removeClass(a); } if (h) { f.find("div.active").addClass("animated " + h); a = h; } else { if (g) { f.find("div.active").addClass("animated " + g); a = g; } else { f.find("div.active").addClass("animated " + b); a = b; } } c = f.find("div.active"); }); } $("a[rel=popover]").popover().click(function (f) { f.preventDefault(); if ($(this).data("easein") != undefined) { $(this).next().removeClass($(this).data("easein")).addClass("animated " + $(this).data("easein")); } else { $(this).next().addClass("animated " + b); } }); });
		$('a[href*="#"]').on('click', function (e) {
			e.preventDefault();

			$('html, body').animate({
				scrollTop: $($(this).attr('href')).offset().top
			}, 500, 'linear');
		});
	}); // End document ready

})(this.jQuery);


function toggleIcon(e) {
	$(e.target)
		.prev('.panel-heading')
		.find(".more-less")
		.toggleClass('glyphicon-plus glyphicon-minus');
}
$('.panel-group').on('hidden.bs.collapse', toggleIcon);
$('.panel-group').on('shown.bs.collapse', toggleIcon);



$(function () {
	$('a[title]').tooltip();
});



jQuery(document).ready(function ($) {

	$('#myCarousel5').carousel({
		interval: 5000
	});

	//Handles the carousel thumbnails
	$('[id^=carousel-selector-]').click(function () {
		var id_selector = $(this).attr("id");
		try {
			var id = /-(\d+)$/.exec(id_selector)[1];
			console.log(id_selector, id);
			jQuery('#myCarousel5').carousel(parseInt(id));
		} catch (e) {
			console.log('Regex failed!', e);
		}
	});
	// When the carousel slides, auto update the text
	$('#myCarousel5').on('slid.bs.carousel', function (e) {
		var id = $('.item.active').data('slide-number');
		$('#carousel-text').html($('#slide-content-' + id).html());
	});
});





